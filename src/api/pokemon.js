export async function pokemonList() {
    const url  = "https://pokeapi.co/api/v2/pokemon?limit=100000&offset=0"
    const res = await fetch (url, {
        method : "GET",
    })
    return res.json();
    
}

export async function pokemonDetail(url) {
    const res = await fetch (url, {
        method : "GET",
    })
    return res.json();
    
}