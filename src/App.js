import './App.css';
import { pokemonDetail, pokemonList } from './api/pokemon';
import { useEffect, useState } from 'react';
import { Image, Badge, ListGroup, Modal, Button, Card, Col, Row, } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  const [pokemons, setPokemons] = useState([])
  const [pdetail, setPdetail] = useState([])
  const [url, setUrl] = useState(false)
  const [show, setShow] = useState(false);
  const [handlerDetail, setHandlerDetail] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    const getPokemon = async () => {
      const data = await pokemonList();
      setPokemons(data.results);
    };
    getPokemon();
    return () => {
    }
  }, [])

  useEffect(() => {
    const handleDetail = async () => {
      const detail = await pokemonDetail(url);
      setPdetail(detail)
      handleShow()
    }
    handleDetail();
    return () => {
    }
  }, [handlerDetail])

  console.log(pokemons)
  return (
    <div className="App">
      <h1>List Of Pokemons (total : {pokemons.length??0} )</h1>
      <Row>
        {pokemons && pokemons.length
          ? pokemons.map((p, index) => (
            <Col md={3} key={index} className="my-2">
              <Card>
                <Card.Body>
                  <Card.Title>{p.name}</Card.Title>
                  <Card.Text>
                    <Button className='btn btn-primary' onClick={() => { setHandlerDetail(!handlerDetail); setUrl(p.url) }}>Details</Button>
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          ))
          : null}
      </Row>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{pdetail.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Image src={pdetail.sprites.back_default}/>
          <Image src={pdetail.sprites.front_default}/>
          <ListGroup>
          <ListGroup.Item className='bg-secondary text-white'><b>Skill(s)</b></ListGroup.Item>
            {pdetail && pdetail.abilities ? pdetail.abilities.map((p, index) => (
              <ListGroup.Item key={index}><span><Badge bg={p.is_hidden ? "secondary" : "success"}>Slot {p.slot} </Badge> | {p.ability.name} | <Badge bg={p.is_hidden ? "secondary" : "success"}> {p.is_hidden ? "Hidden" : "Equipped"} </Badge></span></ListGroup.Item>
            ))
              : null}
            {console.log(pdetail)}
          </ListGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default App;
